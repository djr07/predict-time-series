# Rossmann Stores 
Sales Prediction with Machine Learning

![Sales](https://github.com/djalmajr07/rossmann-sales/blob/main/img/0_7tM5SbKstuED5_AX.jpg)

[![NPM](https://img.shields.io/npm/l/react)](https://github.com/djalmajr07/rossmann-sales/blob/main/LICENSE) 



# Rossmann Prediction Problem

The CFO requests the prediction of each store in a monthly meeting, whereas she was having difficulty to find out the best investment value for the renovations of each store, because the prediction provided by the directors was not assertive, there was a lot of divergence. Therefore to solve this problem, I used Machine Learning algorithms like time series to forecast most precisely how would be the store prediction for the next six weeks (forty-two days).


This solutions has 3 main tasks:

- Monitoring the sales of each store;
- Building a Time Series algorithm to predict the sales amount for the next six weeks;
- Receive on telegram app the predictions in real time.

# How to use this project

## Pre-requirement
- Sing up telegram;
- Create an account;
- Download telegram on desktop or mobile or [telegram web](https://web.telegram.org/).

# Telegram
### Access to Telegram bot

- Open this link and click on send message: https://t.me/RossmannPredSales_bot 
![send_message](https://github.com/djalmajr07/rossmann-sales/blob/main/img/link-telegram-message.PNG)

### Prediction In Real Time
- Send one number per time to recieve the store's prediction based on the next six weeks.


![prediction](https://github.com/djalmajr07/rossmann-sales/blob/main/img/prediction.PNG)


### Pay Attention
-If [Rossmann bot](https://t.me/RossmannPredSales_bot ) recieve anything different from numbers, it will answer back: Store ID is Wrong. On this bot can occur to have some stores not available to have prediction, so it will answer back: Store Not Available. If it happens you can select another Store Id and move on.


![possible-mistakes](https://github.com/djalmajr07/rossmann-sales/blob/main/img/possible-mistakes.PNG)


# Technologies

- Jupyter;
- Python.
 
## Deployment into production
- Back end: Heroku;
- Front end web: Telegram;
- Database: kaggle csv files.

# Author

Djalma Luiz da Silva Junior

https://www.linkedin.com/in/djalmajunior-automa%C3%A7%C3%A3o/


